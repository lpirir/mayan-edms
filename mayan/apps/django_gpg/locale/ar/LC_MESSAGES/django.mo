��            )   �      �     �     �     �     �     �  C   �  *     *   .  )   Y     �     �  A   �  
   �     �     �       4        H     M     T     e     v     z     �     �     �     �  	   �  �  �     z     �     �     �     �  C   �  *   �  *      )   K  
   u     �  A   �  
   �     �     �     �  4        <     H     O     `     q  
   u     �     �     �  
   �  	   �         
                                              	                                                                                   Bad signature. DSA Delete Delete keys Details Document is signed but no public key is available for verification. Document is signed with a valid signature. Document is signed, and signature is good. Document not signed or invalid signature. Download Elgamal Home directory used to store keys as well as configuration files. Import key Import keys from keyservers Key ID Key management Name, e-mail, key ID or key fingerprint to look for. None Public Query key server Query keyservers RSA Search Secret Signature error. Term Type View keys Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-16 22:54+0000
Last-Translator: Yaman Sanobar <yman.snober@gmail.com>
Language-Team: Arabic (http://www.transifex.com/rosarior/mayan-edms/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 Bad signature. DSA حذف Delete keys التفاصيل Document is signed but no public key is available for verification. Document is signed with a valid signature. Document is signed, and signature is good. Document not signed or invalid signature. تحميل Elgamal Home directory used to store keys as well as configuration files. Import key Import keys from keyservers Key ID Key management Name, e-mail, key ID or key fingerprint to look for. لا شيء Public Query key server Query keyservers RSA البحث Secret Signature error. Term النوع View keys 