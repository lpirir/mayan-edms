# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Badea Gabriel <gcbadea@gmail.com>, 2013
# Harald Ersch, 2019
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:30-0400\n"
"PO-Revision-Date: 2019-03-15 21:34+0000\n"
"Last-Translator: Harald Ersch\n"
"Language-Team: Romanian (Romania) (http://www.transifex.com/rosarior/mayan-edms/language/ro_RO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ro_RO\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#: admin.py:24
msgid "None"
msgstr "Nici unul"

#: admin.py:26 links.py:67 models.py:50
msgid "Document types"
msgstr "Tipuri de documente"

#: apps.py:52
msgid "Document indexing"
msgstr "Indexarea documentelor"

#: apps.py:84 models.py:34
msgid "Label"
msgstr "Etichetă"

#: apps.py:85 models.py:39
msgid "Slug"
msgstr "Slug"

#: apps.py:87 apps.py:113 models.py:47 models.py:184
msgid "Enabled"
msgstr "Activat"

#: apps.py:94
msgid "Total levels"
msgstr "Total niveluri"

#: apps.py:100
msgid "Total documents"
msgstr "Total documente"

#: apps.py:109 apps.py:126 apps.py:143
msgid "Level"
msgstr "Nivel"

#: apps.py:119
msgid "Has document links?"
msgstr "Are legături de documente?"

#: apps.py:130 apps.py:149
msgid "Levels"
msgstr "Niveluri"

#: apps.py:134 apps.py:153 models.py:301
msgid "Documents"
msgstr "Documente"

#: forms.py:17
msgid "Indexes to be queued for rebuilding."
msgstr "Indexuri care urmează să fie în coada de așteptare pentru reconstrucție."

#: forms.py:18 links.py:25 links.py:31 links.py:39 links.py:43 models.py:58
#: views.py:88 views.py:249
msgid "Indexes"
msgstr "Indexuri"

#: handlers.py:20
msgid "Creation date"
msgstr "Data creării"

#: links.py:47 views.py:38
msgid "Create index"
msgstr "Creați index"

#: links.py:52 links.py:85
msgid "Edit"
msgstr "Editează"

#: links.py:58 links.py:90
msgid "Delete"
msgstr "Șterge"

#: links.py:62
msgid "Tree template"
msgstr "Arborele șablon"

#: links.py:76
msgid "Deletes and creates from scratch all the document indexes."
msgstr "Șterge și creează de la zero toate indexurile de documente."

#: links.py:78 views.py:370
msgid "Rebuild indexes"
msgstr "Refaceți index-uri"

#: links.py:81
msgid "New child node"
msgstr "Nou nod copil"

#: models.py:38
msgid "This value will be used by other apps to reference this index."
msgstr "Această valoare va fi utilizată de alte aplicații pentru a face referință la acest index."

#: models.py:44
msgid ""
"Causes this index to be visible and updated when document data changes."
msgstr "Cauză pentru acest index să fie vizibil și actualizat când documentul suferă schimbări."

#: models.py:57 models.py:168
msgid "Index"
msgstr "Index"

#: models.py:140
msgid "Index instance"
msgstr "Exemplu de index"

#: models.py:141
msgid "Index instances"
msgstr "Exemple de index-uri"

#: models.py:172
msgid ""
"Enter a template to render. Use Django's default templating language "
"(https://docs.djangoproject.com/en/1.11/ref/templates/builtins/)"
msgstr "Introduceți un șablon pentru a fi afișat. Utilizați limbajul templating implicit al lui Django (https://docs.djangoproject.com/en/1.11/ref/templates/builtins/)"

#: models.py:176
msgid "Indexing expression"
msgstr "Expresie de indexare"

#: models.py:181
msgid "Causes this node to be visible and updated when document data changes."
msgstr "Va face ca acest nod să fie vizibil și actualizat la modificarea datelor documentului."

#: models.py:189
msgid ""
"Check this option to have this node act as a container for documents and not"
" as a parent for further nodes."
msgstr "Bifați această opțiune pentru a avea acest nod ca un container pentru documente și nu ca un părinte pentru nodurile suplimentare."

#: models.py:192
msgid "Link documents"
msgstr "Leagă documente"

#: models.py:196
msgid "Index node template"
msgstr "Șablon pt. nod index"

#: models.py:197
msgid "Indexes node template"
msgstr "Șabloane pt. noduri index"

#: models.py:201
msgid "Root"
msgstr "Rădăcină"

#: models.py:257
#, python-format
msgid ""
"Error indexing document: %(document)s; expression: %(expression)s; "
"%(exception)s"
msgstr "Eroare la indexarea ducumentuluir: %(document)s; expresie: %(expression)s; %(exception)s"

#: models.py:294
msgid "Index template node"
msgstr "Nodul șablonului de index"

#: models.py:297
msgid "Value"
msgstr "Valoare"

#: models.py:307
msgid "Index node instance"
msgstr "Instanță a  nodului index"

#: models.py:308
msgid "Indexes node instances"
msgstr "Instanțele nodurilor index"

#: models.py:406
msgid "Document index node instance"
msgstr "Exemplu de nod index document"

#: models.py:407
msgid "Document indexes node instances"
msgstr "Documentul indexează instanțele de noduri"

#: permissions.py:7 queues.py:8
msgid "Indexing"
msgstr "Indexare"

#: permissions.py:10
msgid "Create new document indexes"
msgstr "Creați un nou index de documente"

#: permissions.py:13
msgid "Edit document indexes"
msgstr "Editați indexul de documente"

#: permissions.py:16
msgid "Delete document indexes"
msgstr "Ștergeți indexul de documente"

#: permissions.py:20
msgid "View document index instances"
msgstr "Vizualizați instanțele indexului de documente"

#: permissions.py:23
msgid "View document indexes"
msgstr "Vezi indexul de documente"

#: permissions.py:26
msgid "Rebuild document indexes"
msgstr "Reconstruire index documente"

#: queues.py:12
msgid "Delete empty index nodes"
msgstr "Ștergeți nodurile index goale"

#: queues.py:16
msgid "Remove document"
msgstr "Eliminați documentul"

#: queues.py:20
msgid "Index document"
msgstr "Indexați documentul"

#: queues.py:24
msgid "Rebuild index"
msgstr "Refaceți indexul"

#: views.py:53
#, python-format
msgid "Delete the index: %s?"
msgstr "Ștergeți indexul: %s?"

#: views.py:66
#, python-format
msgid "Edit index: %s"
msgstr "Editați indexul: %s"

#: views.py:82
msgid ""
"Indexes group document automatically into levels. Indexe are defined using "
"template whose markers are replaced with direct properties of documents like"
" label or description, or that of extended properties like metadata."
msgstr "Indexurile grupează documentele automat în nivele. Indexurile sunt definite folosind un șablon al cărui marcatori sunt înlocuiți cu proprietăți directe ale unor documente, cum ar fi eticheta sau descrierea, sau cele ale unor proprietăți extinse precum metadatele."

#: views.py:87
msgid "There are no indexes."
msgstr "Nu există indexuri."

#: views.py:94
msgid "Available document types"
msgstr "Tipuri de documente disponibile"

#: views.py:96
msgid "Document types linked"
msgstr "Tipuri de documente legate"

#: views.py:111
#, python-format
msgid "Document types linked to index: %s"
msgstr "Tipuri de documente legate de index: %s"

#: views.py:114
msgid ""
"Only the documents of the types selected will be shown in the index when "
"built. Only the events of the documents of the types select will trigger "
"updates in the index."
msgstr "Numai documentele tipurilor selectate vor fi afișate în index atunci când sunt construite. Doar evenimentele din documentele selectate vor declanșa actualizări ale indexului."

#: views.py:147
#, python-format
msgid "Tree template nodes for index: %s"
msgstr "Arborele nod șablon pentru index: %s"

#: views.py:177
#, python-format
msgid "Create child node of: %s"
msgstr "Creați un nod copil al: %s"

#: views.py:201
#, python-format
msgid "Delete the index template node: %s?"
msgstr "Ștergeți nodul șablon index: %s?"

#: views.py:223
#, python-format
msgid "Edit the index template node: %s?"
msgstr "Editați nodul șablonului index: %s?"

#: views.py:244
msgid ""
"This could mean that no index templates have been created or that there "
"index templates but they are no properly defined."
msgstr "Acest lucru ar putea însemna că nu au fost create șabloane de index sau că există șabloane index, dar nu sunt definite în mod corespunzător."

#: views.py:248
msgid "There are no index instances available."
msgstr "Nu există instanțe index disponibile."

#: views.py:290
#, python-format
msgid "Navigation: %s"
msgstr "Navigare: %s"

#: views.py:295
#, python-format
msgid "Contents for index: %s"
msgstr "Conținutul pentru index: %s"

#: views.py:349
msgid ""
"Assign the document type of this document to an index to have it appear in "
"instances of those indexes organization units. "
msgstr "Atribuiți tipul de document al acestui document într-un index pentru a fi afișat în instanțele unităților de organizare a acestor indici."

#: views.py:354
msgid "This document is not in any index"
msgstr "Acest document nu este în nici un index"

#: views.py:358
#, python-format
msgid "Indexes nodes containing document: %s"
msgstr "Nodurile indexurilor care conțin documentul: %s"

#: views.py:384
#, python-format
msgid "%(count)d index queued for rebuild."
msgid_plural "%(count)d indexes queued for rebuild."
msgstr[0] "Indicele %(count)d se află în coada de așteptare pentru reconstrucție."
msgstr[1] "%(count)d indexate în coada pentru reconstrucție."
msgstr[2] "%(count)d indexări în coada pentru reconstrucție."
